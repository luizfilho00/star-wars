# Star Wars API

Projeto desenvolvido em Flutter para dispositivos móveis utilizando a linguagem Dart 2.
<br />Consome a API SWAPI: (https://swapi.co)


**Funções implementadas**

 - [x] Listar filmes
 - [x] Busca de filmes
 - [x] Exibir detalhes sobre um filme
 - [x] Exibir detalhes sobre personagens do filme
 - [x] Exibir detalhes sobre naves do filme
 - [x] Exibir detalhes sobre espécies do filme
 - [x] Exibir detalhes sobre veículos do filme
 - [x] Exibir detalhes sobre planetas do filme

